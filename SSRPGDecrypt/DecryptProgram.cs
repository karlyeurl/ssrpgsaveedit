﻿using System;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace SSRPGDecrypt
{
    class Program
    {
        private const int Keysize = 256;
        private const int DerivationIterations = 1000;

        static void Main(string[] args)
        {

            StreamReader sr = new StreamReader("primary_save.txt");
            
            // Read the stream to a string, and write the string to the console.
            Console.WriteLine("Reading progress_data from primary_save.txt");
            string line;  
            while((line = sr.ReadLine()) != null)  
            {  
                if(line.StartsWith("progress_data:")) {
                    int idx = line.IndexOf(":");
                    line = line.Substring(idx + 1, line.Length-idx-2);
                    break;
                }
            }  

            
            string passPhrase = "peekabeyoufoundme";
            string save = line;

            var array = Convert.FromBase64String(save);
            byte[] salt = array.Take(32).ToArray();
            byte[] rgbIV = array.Skip(32).Take(32).ToArray();
            byte[] array2 = array.Skip(64).Take(array.Length - 64).ToArray();
            byte[] bytes = new Rfc2898DeriveBytes(passPhrase, salt, 1000).GetBytes(32);

            Console.WriteLine("Dumping IV and key for later use...");
            File.WriteAllBytes("iv.dat", rgbIV);
            File.WriteAllBytes("salt.dat", salt);

            Console.WriteLine("Deciphering...");
            using (RijndaelManaged rijndaelManaged = new RijndaelManaged())
            {
                rijndaelManaged.BlockSize = 256;
                rijndaelManaged.Mode = CipherMode.CBC;
                rijndaelManaged.Padding = PaddingMode.PKCS7;
                using (ICryptoTransform transform = rijndaelManaged.CreateDecryptor(bytes, rgbIV))
                {
                    using (MemoryStream memoryStream = new MemoryStream(array2))
                    {
                        using (CryptoStream cryptoStream = new CryptoStream(memoryStream, transform, CryptoStreamMode.Read))
                        {
                            byte[] array3 = new byte[array2.Length];
                            int count = cryptoStream.Read(array3, 0, array3.Length);
                            memoryStream.Close();
                            cryptoStream.Close();
                            //Console.WriteLine(Encoding.UTF8.GetString(array3, 0, count));
                            var logFile = System.IO.File.Create("plaintext.txt");
                            var logWriter = new System.IO.StreamWriter(logFile);
                            logWriter.WriteLine(Encoding.UTF8.GetString(array3, 0, count));
                            logWriter.Dispose();
                            Console.WriteLine("Written plaintext to plaintext.txt.");
                            Console.WriteLine("You may edit it as you see fit and run the SSRPGEncrypt part.");
                        }
                    }
                }
            }
        }
    }
}