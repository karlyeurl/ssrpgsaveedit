# Stone Story RPG Save editor project

This project is a crude hack cobbled together to load a save, extract the save data to text, and then load the altered text to a new save.

## How to build

Honestly, I don't know. Whatever floats your boat best. Here are the steps I took on Linux:

```
$ dotnet new console
```

This will create the basic structure for the dotnet app to build the DLL. The replace `Program.cs` with the existing `Decrypt/EncryptProgram.cs`

```
$ mv DecryptProgram.cs Program.cs
```

Then build the app:

```
$ dotnet build
```

This will create a file in `bin/Debug/netcoreapp2.2/SSRPGDecrypt.dll`: that is your executable.

## How to run

Place the save file (currently called `primary_save.txt`) in the directory. Then run:

```
$ mono bin/Debug/netcoreapp2.2/SSRPGDecrypt.dll
```

`mono` emulates the .NET Framework, which is different from the .NET Core and provides the feature needed for decryption and encryption with Rijndael-256 (this is *not* AES).

On the decryptor side, this will create a file called `plaintext.txt`. Edit this file to your liking. Try to not alter the structure too much, as it may not work. Use small, simple, incremental changes.

Then do the same build process for the encryptor, and run the following command from a directory which holds `primary_save.txt`, `iv.dat`, `salt.dat` and `plaintext.txt`:

```
$ mono bin/Debug/netcoreapp2.2/SSRPGEncrypt.dll
```

For convenience, the DLLs are also provided as is, as compiled binaries.
