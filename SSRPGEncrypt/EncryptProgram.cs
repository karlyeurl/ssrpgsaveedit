﻿using System;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace SSRPGEncrypt
{
    class Program
    {
        static void Main(string[] args)
        {
            byte[] rgbIV = File.ReadAllBytes("iv.dat");
            byte[] salt = File.ReadAllBytes("salt.dat");
            StreamReader ptr = new StreamReader("plaintext.txt");
            string plaintext = ptr.ReadToEnd();
            byte[] bytes = Encoding.UTF8.GetBytes(plaintext);
            string passPhrase = "peekabeyoufoundme";

            byte[] key = new Rfc2898DeriveBytes(passPhrase, salt, 1000).GetBytes(32);
            string encryptedSave;

            using (RijndaelManaged rijndaelManaged = new RijndaelManaged())
            {
                rijndaelManaged.BlockSize = 256;
                rijndaelManaged.Mode = CipherMode.CBC;
                rijndaelManaged.Padding = PaddingMode.PKCS7;
                using (ICryptoTransform transform = rijndaelManaged.CreateEncryptor(key, rgbIV))
                {
                    using (MemoryStream memoryStream = new MemoryStream())
                    {
                        using (CryptoStream cryptoStream = new CryptoStream(memoryStream, transform, CryptoStreamMode.Write))
                        {
                            cryptoStream.Write(bytes, 0, bytes.Length);
                            cryptoStream.FlushFinalBlock();
                            byte[] inArray = salt.Concat(rgbIV).ToArray().Concat(memoryStream.ToArray())
                                .ToArray();
                            memoryStream.Close();
                            cryptoStream.Close();
                            encryptedSave = Convert.ToBase64String(inArray);
                        }
                    }
                }
            }

            StreamReader sr = new StreamReader("primary_save.txt");
            StreamWriter sw = new StreamWriter("primary_save_altered.txt");
            string line;  
            while((line = sr.ReadLine()) != null)  
            {  
                if(line.StartsWith("progress_data:")) {
                    string altered_line = "progress_data:" + encryptedSave + ",";
                    sw.WriteLine(altered_line);
                } else {
                    sw.WriteLine(line);
                }
            }
            sw.Close();
            Console.WriteLine("Written altered save file to primary_save_altered.txt.");
            Console.WriteLine("You may put it back to wherever your original save was and start the game.");
        }
    }
}
